package it.iad2.negozio;

import java.sql.*;

public class SqlTable {
    public static void createTable(){
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String sql = " CREATE TABLE `prodottialimentari` (\n" +
                    "  `code` varchar(20) NOT NULL,\n" +
                    "  `nome` varchar(20) NOT NULL,\n" +
                    "  `descrizione` varchar(200) NOT NULL,\n" +
                    "  `prezzo` double NOT NULL,\n" +
                    "  `dataScadenza` date NOT NULL\n" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;\n";


            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set

            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }
    public static void createTablenon(){
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String sql = "CREATE TABLE `prodottinonalimentari` (\n" +
                    "  `code` varchar(20) NOT NULL,\n" +
                    "  `nome` varchar(20) NOT NULL,\n" +
                    "  `descrizione` varchar(200) NOT NULL,\n" +
                    "  `prezzo` double NOT NULL,\n" +
                    "  `materiale` varchar(20) NOT NULL\n" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;\n";


            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set

            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }
}
