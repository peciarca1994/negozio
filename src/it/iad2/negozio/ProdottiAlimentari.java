package it.iad2.negozio;


import java.sql.*;
import java.util.Scanner;

public class ProdottiAlimentari extends Prodotti {
    java.sql.Date dataScadenza;

    public ProdottiAlimentari(String code, String nome, String descrizione, Double prezzo, java.sql.Date dataScadenza) {
        super(code, nome, descrizione, prezzo, dataScadenza);
        this.dataScadenza = dataScadenza;
    }

    public Date getDataScadenza() {
        return dataScadenza;
    }

    public void createOggettoAl() {
        System.out.println("inserisci Barcode:");
        Scanner code = new Scanner(System.in);
        String codes = code.nextLine();
        System.out.println("inserisci Nome:");
        Scanner nome = new Scanner(System.in);
        String nomes = nome.nextLine();
        System.out.println("inserisci Prezzo:");
        Scanner prezzo = new Scanner(System.in);
        double prezzos = prezzo.nextDouble();
        System.out.println("inserisci Descrizione:");
        Scanner descrizione = new Scanner(System.in);
        String descriziones = descrizione.nextLine();
        System.out.println("inserisci Giorno Scadenza:");
        Scanner day = new Scanner(System.in);
        int days = day.nextInt();
        System.out.println("inserisci Mese Scadenza:");
        Scanner month = new Scanner(System.in);
        int months = month.nextInt();
        System.out.println("inserisci Anno Scadenza:");
        Scanner year = new Scanner(System.in);
        int years = year.nextInt();
        java.sql.Date data = new java.sql.Date(years - 1900, months - 1, days);
    }

    public static void addProdottoAlimentare(ProdottiAlimentari prodottiAlimentari) {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String codes = prodottiAlimentari.getCode();
            String nomes = prodottiAlimentari.getNome();
            String descriziones = prodottiAlimentari.getDescrizione();
            double prezzos = prodottiAlimentari.getPrezzo();
            java.sql.Date data = prodottiAlimentari.getDataScadenza();
            //java.sql.Date data = new java.sql.Date(years-1900,months - 1,days);
            String sql = "INSERT INTO `prodottialimentari` (`code`, `nome`, `descrizione`, `prezzo`, `dataScadenza`) VALUES ('" + codes + "', '" + nomes + "', '" + descriziones + "', '" + prezzos + "', '" + data + "')";


            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                System.out.println("Prodotto inserito Correttamente:");

                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }

    public static void visualizzaProdottiAl() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String sql = "SELECT `code`, `nome` FROM `prodottialimentari` WHERE 1";


            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);
// Extract data from result set
                System.out.println("\n-------------PRODOTTI ALIMENTARI-------------\n");
                while (rs.next()) {
                    //Retrieve by column name
                    String name = rs.getString("nome");
                    String code = rs.getString("code");

                    //Display values
                    System.out.println("Nome: " + name);
                    System.out.println("Codice: " + code);
                }
                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }

    public static void selectPerCodeAl() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            System.out.println("Inserisci Barcode del prodotto:");
            Scanner code = new Scanner(System.in);
            String codes = code.nextLine();
            String sql = "SELECT * FROM `prodottialimentari` WHERE `code`= '" + codes + "'";


            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                while (rs.next()) {
                    //Retrieve by column name
                    String name = rs.getString("nome");
                    String cod = rs.getString("code");
                    String des = rs.getString("descrizione");
                    double prez = rs.getDouble("prezzo");
                    Date date = rs.getDate("dataScadenza");

                    //Display values
                    System.out.println("Nome: " + name);
                    System.out.println("Codice: " + cod);
                    System.out.println("Decrizione: " + des);
                    System.out.println("Prezzo: " + prez+ "€");
                    System.out.println("Data di Scadenza: " + date);
                    java.util.Date data = new java.util.Date();

                    //(dataScadenza - dataDiAdesso (sono long))/86400000L = Giorni di differenza
                    if (((date.getTime() - data.getTime()) / 86400000L) <= 0) {
                        System.out.println("E' Scaduto");
                        break;
                    } else if (((date.getTime() - data.getTime()) / 86400000L) <= 5) {
                        System.out.println("Prezzo con Sconto:" + prez * 50 / 100+"€");
                        break;
                    } else
                        prez -= prez * 20 / 100;
                    System.out.println("Prezzo con Sconto:" + prez+"€");
                    break;
                }

                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }


    public static void selectProdottiInScadenza() {

        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String sql = "SELECT * FROM `prodottialimentari` WHERE (DATEDIFF( `dataScadenza`, now())) <= 5";


            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                while (rs.next()) {
                    //Retrieve by column name
                    String name = rs.getString("nome");
                    String cod = rs.getString("code");
                    String des = rs.getString("descrizione");
                    double prez = rs.getDouble("prezzo");
                    Date date = rs.getDate("dataScadenza");

                    //Display values
                    System.out.println("Nome: " + name);
                    System.out.println("Codice: " + cod);
                    System.out.println("Decrizione: " + des);
                    System.out.println("Prezzo: " + prez);
                    System.out.println("Data di Scadenza: " + date);
                }
                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }


    }

    public static void deleteProdottoAl() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            System.out.println("Selezionare Barcode del Prodotto Alimentare da Eliminare");
            Scanner codeDelet = new Scanner(System.in);
            String codeDelete = codeDelet.nextLine();
            String sql = "DELETE FROM `prodottialimentari` WHERE `code`= '" + codeDelete + "'";

            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                System.out.println("Prodotto Cancellato Correttamente:");

                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }

    }

    public static void updateProdottiAl() {
        System.out.println("Inserisci il Barcode del prodotto Alimentare da modificare:");
        Scanner barcode = new Scanner(System.in);
        String barcodes = barcode.nextLine();
        System.out.println("inserisci Barcode Aggiornato:");
        Scanner code = new Scanner(System.in);
        String codes = code.nextLine();
        System.out.println("inserisci Nome Aggiornato:");
        Scanner nome = new Scanner(System.in);
        String nomes = nome.nextLine();
        System.out.println("inserisci Prezzo Aggiornato:");
        Scanner prezzo = new Scanner(System.in);
        double prezzos = prezzo.nextDouble();
        System.out.println("inserisci Descrizione Aggiornata:");
        Scanner descrizione = new Scanner(System.in);
        String descriziones = descrizione.nextLine();
        System.out.println("inserisci Giorno Scadenza Aggiornato:");
        Scanner day = new Scanner(System.in);
        int days = day.nextInt();
        System.out.println("inserisci Mese Scadenza Aggiornato:");
        Scanner month = new Scanner(System.in);
        int months = month.nextInt();
        System.out.println("inserisci Anno Scadenza Aggiornato:");
        Scanner year = new Scanner(System.in);
        int years = year.nextInt();
        java.sql.Date dataScadenzas = new java.sql.Date(years - 1900, months - 1, days);
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String sql = "UPDATE `prodottialimentari` SET `code`='" + codes + "',`nome`='" + nomes + "',`descrizione`='" + descriziones + "',`prezzo`='" + prezzos + "',`dataScadenza`='" + dataScadenzas + "' WHERE `code`= '" + barcodes + "'";


            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                System.out.println("Prodotto aggiornato Correttamente:");

                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }

    }
}













