package it.iad2.negozio;

import java.sql.*;

public class Prodotti {
    String code;
    String nome;
    String descrizione;
    Double prezzo;

    public Prodotti(String code, String nome, String descrizione, Double prezzo, java.sql.Date dataScadenza) {
        this.code = code;
        this.nome = nome;
        this.descrizione = descrizione;
        this.prezzo = prezzo;
    }

    public String getCode() {
        return code;
    }

    public String getNome() {
        return nome;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public Double getPrezzo() {
        return prezzo;
    }

    public Prodotti(String code, String nome, String descrizione, Double prezzo, String materiale) {
        this.code = code;
        this.nome = nome;
        this.descrizione = descrizione;
        this.prezzo = prezzo;
    }

    public static void formatDatabase() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String sql = "DELETE FROM `prodottinonalimentari` WHERE 1\n" ;



            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                System.out.println("Database Formattato");
                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }
    public static void formatDatabase2() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String sql = "DELETE FROM `prodottialimentari` WHERE 1\n" ;



            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                System.out.println("Database Formattato");
                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }


}
