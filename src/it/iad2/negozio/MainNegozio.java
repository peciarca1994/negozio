package it.iad2.negozio;
/*
Questa esercitazione si basa sul concetto di ereditarietà nell'ambito Java.
Descrizione:
Il gestore di un negozio associa a tutti i suoi prodotti
un nome
	un codice univoco
	una descrizione sintetica del prodotto
	il suo prezzo unitario.
Il gestore del negozio vuole fare una distinzione tra i prodotti Alimentari e quelli Non Alimentari.
Ai prodotti Alimentari viene associata una data di scadenza, mentre per quelli Non Alimentari viene
specificato il materiale principale di cui sono composti.
Ad ogni prodotto è associato uno sconto del 5%. Essendo in periodo post covid, i generi alimentari
hanno uno sconto del 20% invece che del 5% e ,se la data di scadenza è inferiore a 5 giorni, lo sconto
diventa del 50%.
Requisiti
Il programma dovrà:
    Stampare la lista dei Prodotti (nome prodotto e codice)
	Selezionare un prodotto in base al codice e stamparne descrizione e prezzo con e senza sconto.
	Selezionare tutti i prodotti che sono in scadenza.
 */

import java.util.Scanner;

public class MainNegozio {
    public static void main(String[] args) {
        SqlTable.createTable();
        SqlTable.createTablenon();
        String switchNumber = "";
        while (!switchNumber.equals("exit")) {
            System.out.println("INSERIRE:");
            System.out.println(" 1 per stampa prodotti");
            System.out.println(" 2 per aggiungere un prodotto Alimentare");
            System.out.println(" 3 per aggiungere un prodotto Non Alimentare");
            System.out.println(" 4 per selezionare prodotto Non Alimentare in base al codice");
            System.out.println(" 5 per selezionare prodotto Alimentare in base al codice");
            System.out.println(" 6 per vedere prodotti in scadenza");
            System.out.println(" 7 per cancellare un prodotto Alimentare");
            System.out.println(" 8 per cancellare un prodotto Non Alimentare");
            System.out.println(" 9 per aggiornare un prodotto Alimentare");
            System.out.println("10 per aggiornare un prodotto Non Alimentare");
            System.out.println("11 per reset del DataBase");
            System.out.println("exit per uscire dall'applicazione");
            Scanner a = new Scanner(System.in);
            switchNumber = a.nextLine();
            switch (switchNumber) {
                //Stampa La lista dei prodotti
                case "1":
                    ProdottiNonAlimentari.visualizzaProdottiNonAlimentare();
                    ProdottiAlimentari.visualizzaProdottiAl();

                    break;
                //aggiunge prodotto alimentare
                case "2": {
                    System.out.println("inserisci Barcode:");
                    Scanner code = new Scanner(System.in);
                    String codes = code.nextLine();
                    System.out.println("inserisci Nome:");
                    Scanner nome = new Scanner(System.in);
                    String nomes = nome.nextLine();
                    System.out.println("inserisci Prezzo:");
                    Scanner prezzo = new Scanner(System.in);
                    double prezzos = prezzo.nextDouble();
                    System.out.println("inserisci Descrizione:");
                    Scanner descrizione = new Scanner(System.in);
                    String descriziones = descrizione.nextLine();
                    System.out.println("inserisci Giorno Scadenza:");
                    Scanner day = new Scanner(System.in);
                    int days = day.nextInt();
                    System.out.println("inserisci Mese Scadenza:");
                    Scanner month = new Scanner(System.in);
                    int months = month.nextInt();
                    System.out.println("inserisci Anno Scadenza:");
                    Scanner year = new Scanner(System.in);
                    int years = year.nextInt();
                    java.sql.Date dataScadenzas = new java.sql.Date(years - 1900, months - 1, days);

                    ProdottiAlimentari pA = new ProdottiAlimentari(codes, nomes, descriziones, prezzos, dataScadenzas);
                    ProdottiAlimentari.addProdottoAlimentare(pA);

                    break;
                }
                //aggiunge prodotto non alimentare
                case "3":
                    System.out.println("inserisci Barcode:");
                    Scanner code = new Scanner(System.in);
                    String codes = code.nextLine();
                    System.out.println("inserisci Nome:");
                    Scanner nome = new Scanner(System.in);
                    String nomes = nome.nextLine();
                    System.out.println("inserisci Prezzo:");
                    Scanner prezzo = new Scanner(System.in);
                    double prezzos = prezzo.nextDouble();
                    System.out.println("inserisci Descrizione:");
                    Scanner descrizione = new Scanner(System.in);
                    String descriziones = descrizione.nextLine();
                    System.out.println("inserisci Materiale:");
                    Scanner materiale = new Scanner(System.in);
                    String materiales = materiale.nextLine();


                    ProdottiNonAlimentari prodottiNonAlimentari = new ProdottiNonAlimentari(codes, nomes, descriziones, prezzos, materiales);
                    ProdottiNonAlimentari.addProdottoNonAlimentare(prodottiNonAlimentari);

                    break;
                //stampa prodottiNonAl in base al code
                case "4":
                    ProdottiNonAlimentari.selectPerCodeNonAl();

                    break;
                //stampa prodottiAl in base al code
                case "5":
                    ProdottiAlimentari.selectPerCodeAl();
                    break;

                //stampa prodotti in scadenza
                case "6":
                    ProdottiAlimentari.selectProdottiInScadenza();

                    break;
                //cancella prod al in base al barcode
                case "7":
                    ProdottiAlimentari.deleteProdottoAl();
                    break;
                //cancella prod non al in base al barcode
                case "8":
                    ProdottiNonAlimentari.deleteProdottoNonAl();
                    break;
                //aggiorna un prodotto al selezionandolo tramite barcode
                case "9":
                    ProdottiAlimentari.updateProdottiAl();
                    break;
                //aggiorna un prodotto non al selezionandolo tramite barcode
                case "10":
                    ProdottiNonAlimentari.updateProdottiNonAl();
                    break;
                case "11":
                    String secure = "";
                    while (!secure.equals("Si") || !secure.equals(("No"))) {
                        System.out.println("Sei sicuro di voler formattare il DataBase Si/No");
                        Scanner choice = new Scanner(System.in);
                        secure = choice.nextLine();

                        switch (secure) {

                            case "Si":
                                Prodotti.formatDatabase();
                                Prodotti.formatDatabase2();
                                break;
                            case "No":
                                System.out.println("Database non Formattato");
                                break;
                            default:
                                System.out.println("Selezione non Corretta\n\n\nRiprova: \n\n");
                                continue;
                        }
                        break;
                    }
                    break;
                case "exit":
                    System.out.println("Arrivederci");
                    break;
                default:
                    System.out.println("inserire un numero tra quelli proposti...");
                    break;

            }

        }

    }
}

