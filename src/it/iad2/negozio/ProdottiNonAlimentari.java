package it.iad2.negozio;

import java.sql.*;
import java.util.Scanner;

public class ProdottiNonAlimentari extends Prodotti {
    String materiale;

    public String getMateriale() {
        return materiale;
    }

    public ProdottiNonAlimentari(String code, String nome, String descrizione, Double prezzo, String materiale) {
        super(code, nome, descrizione, prezzo, materiale);
        this.materiale = materiale;
    }

    public static void addProdottoNonAlimentare(ProdottiNonAlimentari prodottiNonAlimentari) {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String codes = prodottiNonAlimentari.getCode();
            String nomes = prodottiNonAlimentari.getNome();
            String descriziones = prodottiNonAlimentari.getDescrizione();
            double prezzos = prodottiNonAlimentari.getPrezzo();
            String materiales = prodottiNonAlimentari.getMateriale();
            String sql = "INSERT INTO `prodottinonalimentari` (`code`, `nome`, `descrizione`, `prezzo`, `materiale`) VALUES ('" + codes + "', '" + nomes + "', '" + descriziones + "', '" + prezzos + "', '" + materiales + "')";


            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                System.out.println("Prodotto inserito Correttamente:");

                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }

    public static void visualizzaProdottiNonAlimentare() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String sql = "SELECT `code`, `nome` FROM `prodottinonalimentari` WHERE 1";


            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);
// Extract data from result set
                System.out.println("\n-------------PRODOTTI NON ALIMENTARI-------------\n");
                while (rs.next()) {
                    //Retrieve by column name
                    String name = rs.getString("nome");
                    String code = rs.getString("code");

                    //Display values
                    System.out.println("Nome: " + name);
                    System.out.println("Codice: " + code);
                }
                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }

    public static void selectPerCodeNonAl() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            System.out.println("Inserisci Barcode del prodotto:");
            Scanner code = new Scanner(System.in);
            String codes = code.nextLine();
            String sql = "SELECT * FROM `prodottinonalimentari` WHERE `code`= '" + codes + "'";


            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                while (rs.next()) {
                    //Retrieve by column name
                    String name = rs.getString("nome");
                    String cod = rs.getString("code");
                    String des = rs.getString("descrizione");
                    double prez = rs.getDouble("prezzo");
                    String materiale = rs.getString("materiale");

                    //Display values
                    System.out.println("Nome: " + name);
                    System.out.println("Codice: " + cod);
                    System.out.println("Decrizione: " + des);
                    System.out.println("Prezzo: " + prez + "€");
                    System.out.println("Materiale: " + materiale);
                    prez -= prez * 5 / 100;
                    System.out.println("Prezzo con Sconto:" + prez+"€");
                }

                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }
    }

    public static void deleteProdottoNonAl() {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            System.out.println("Selezionare Barcode del Prodotto Non Alimentare da Eliminare");
            Scanner codeDelet = new Scanner(System.in);
            String codeDelete = codeDelet.nextLine();
            String sql = "DELETE FROM `prodottinonalimentari` WHERE `code`= '" + codeDelete + "'";

            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                System.out.println("Prodotto Cancellato Correttamente:");

                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }

    }

    public static void updateProdottiNonAl() {
        System.out.println("Inserisci il Barcode del prodotto Non Alimentare da modificare:");
        Scanner barcode = new Scanner(System.in);
        String barcodes = barcode.nextLine();
        System.out.println("inserisci Barcode Aggiornato:");
        Scanner code = new Scanner(System.in);
        String codes = code.nextLine();
        System.out.println("inserisci Nome Aggiornato:");
        Scanner nome = new Scanner(System.in);
        String nomes = nome.nextLine();
        System.out.println("inserisci Prezzo Aggiornato:");
        Scanner prezzo = new Scanner(System.in);
        double prezzos = prezzo.nextDouble();
        System.out.println("inserisci Descrizione Aggiornata:");
        Scanner descrizione = new Scanner(System.in);
        String descriziones = descrizione.nextLine();
        System.out.println("inserisci Materiale Aggiornato:");
        Scanner materiale = new Scanner(System.in);
        String materiales = materiale.nextLine();

        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?user=root&password=");
            // Run SQL -> start from here
            Statement stmt = null;
            ResultSet rs = null;
            String sql = "UPDATE `prodottinonalimentari` SET `code`='" + codes + "',`nome`='" + nomes + "',`descrizione`='" + descriziones + "',`prezzo`='" + prezzos + "',`dataScadenza`='" + materiales + "' WHERE `code`= '" + barcodes + "'";


            try {
                stmt = conn.createStatement();
                stmt.execute(sql);
// Extract data from result set
                System.out.println("\n-------------SQL DATA-------------\n");
                System.out.println("Prodotto aggiornato Correttamente:");

                System.out.println("\n\n-------------END-------------\n");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    rs = null;
                }

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
// SQL end at here
        } catch (Exception ex) {
            System.err.println("Cannot connect to database server");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
///System.out.println("\n***** Let terminate the Connection *****");
                    conn.close();
                    // System.out.println ("\n\nDatabase connection terminated...");
                } catch (Exception ex) {
                    System.out.println("Error in connection termination!");
                }
            }
        }

    }
}
